import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import HelloWorld from "../components/HelloWorld.vue";
import HMICellMachineLister from "../components/HMICellMachineLister.vue";
import NotFound from "../views/NotFound.vue";
import GaugeDemo from "../views/GaugeDemo.vue";
import MyGauge from "../components/MyGauge.vue";
import DecisionMaker from "../components/DecisionMaker.vue";
import EmitterDemo from "@/views/EmitterDemo.vue";

Vue.use(VueRouter);

const routes = [
    { path: "/", name: "Home", component: Home },
    { path: '/HelloWorld/:myMessage', name: 'HelloWorld', component: HelloWorld },
    { path: '/HMICellMachineLister/:host', name: 'HMICellMachineLister', component: HMICellMachineLister },
    { path: '/GaugeDemo', name: 'MyGaugeDemoName', component: GaugeDemo },
    { path: '/MyGauge', name: 'MyGauge', component: MyGauge },
    { path: '/DecisionMaker', name: 'DecisionMaker', component: DecisionMaker },
    { path: '/EmitterDemo', name: 'EmitterDemo', component: EmitterDemo },
    { path: "/about", name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
        import(/* webpackChunkName: "about" */ "../views/About.vue")
    },
    { path: '*', component: NotFound }, //Always keep wildcard at the end of this array
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;