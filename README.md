# vueplayground
Demonstration showing basics of vue development and framework features.

## Project setup  
(Download and install all dependencies defined in packages.json)  
```
npm install
```


### Compiles and hot-reloads for development  
(Uses Node.js to stand-up a web server and development enviromment)  
```
npm run serve
```


### Compiles and minifies for production
(Check the /dist folder in project root after running this. This is your deployment package)  
```
npm run build
```


### Lints and fixes files
(Static analyis and coding standard review)
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
=======
# README #

### If installing a linux development environment from scratch, you can use the following install history log as a guide/template: ###
  
```
9  sudo apt install curl
10  curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash - sudo apt-get install -y nodejs
11  curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -sudo apt-get install -y nodejs
12  curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash - sudo apt-get install -y nodejs
13  curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
14  sudo apt-get install -y nodejs && sudo apt-get install gcc g++ make
15  node
16  npm
...
18  sudo npm install -g @vue/cli
19  vue --version
20  sudo npm update -g @vue/cli
21  ll
22  mkdir vue
23  cd vue
24  vue create test
25  ll
26  cd test
…
31  npm run serve
32  vue --version
33  vue --help
34  vue ui
```
